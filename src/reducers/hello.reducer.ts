import { Action } from '@ngrx/store';
import { SPANISH, ENGLISH, FRENCH } from '../actions/hello.actions';

export function HelloReducer(state = 'Hello Redux', action: Action) {
  switch(action.type) {
    case SPANISH:  
      return 'Hola Redux';
    case ENGLISH:  
      return 'Hello Redux';
    case FRENCH:
      return 'Benjour Redux';
    default:
      return state;
  }
}
