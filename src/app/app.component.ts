import { Component } from '@angular/core';
import { Store } from "@ngrx/store";
import { Observable } from "rxjs";
import { AppState } from '../states/hello.state';
import * as Actions from '../actions/hello.actions';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  message: Observable<string>;

  constructor(private store: Store<AppState>){
    this.message = this.store.select('message')
  }

  spanishMessage(){
    this.store.dispatch({ type: Actions.SPANISH })
  }

  frenchMessage(){
    this.store.dispatch({ type: Actions.FRENCH });
  }

  englishMessage(){
    this.store.dispatch({ type: Actions.ENGLISH })
  }
}
