import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { HelloReducer } from "../reducers/hello.reducer";
import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule, 
    StoreModule.forRoot({
      message: HelloReducer
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
